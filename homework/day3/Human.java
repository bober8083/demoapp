package homework.day3;

public interface Human extends CanAccepMoney {
    void see();
    void eat(Food food);
    void talk();
    void sleep();
}
